import cgitb
import cgi
import requests
from xml.etree import ElementTree as ET
import json

urlAchat='http://localhost:8080/APISOAP/services/Gateway'
Header = {"Content-Type":"text/xml","SOAPAction":""}
form = cgi.FieldStorage()

def rewriteJson():# fonction de sauvegarde de la base
    liste=[]
    with open('data.json') as json_data:
        data = json.load(json_data)
        for meuble in data["meuble"]:
            if int(meuble["id"])==int(form["objet"].value):
                meuble["quantite"]=int(meuble["quantite"])-1
                liste.append(meuble)
            else:
                liste.append(meuble)
    with open('data.json', 'w') as fp:
        dict={}
        dict["meuble"]=liste
        json.dump(dict, fp)

print("Content-Type: text/html")
print() 
print("<title>achat</title>")
print("<div>")
print("<h1> page d'achat </h1>")


body=("""
<soapenv:Envelope
	xmlns:q0="http://webservices"
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<soapenv:Header>
    </soapenv:Header>
	<soapenv:Body>
    <q0:removeMoney>
    <q0:numCompte>{}</q0:numCompte>
    <q0:codeCompte>{}</q0:codeCompte>
    <q0:montant>{}</q0:montant>
    </q0:removeMoney>
    </soapenv:Body>
    </soapenv:Envelope>
""")

if "result" not in form:#si l'achat na pas encore etait effectué
    with open('data.json') as json_data:
        data = json.load(json_data)
        for meuble in data["meuble"]:
            if int(meuble["id"])==int(form["objet"].value):
                print("""<image src="{}" width={} height={}/> """.format(meuble["image"],meuble["width"],meuble["height"]))
                print("<p>nom de l'objet :{}<br/>prix :{}<br/> quantité restante : {}</p>".format(meuble["nom"],meuble["prix"],meuble['quantite']))
                print(""" <form action="/achat.py?result=1" method="post"> 
                <input required type="text" name="numCompte" placeholder="numero de compte"/>
                <input required type="password" name="codeCompte" placeholder="code secret"/>      
                <input type="hidden" name="montant" value="{}" />
                <input type="hidden" name="objet" value="{}" />
                <input type="submit" name="send" value="acheter">
                </form> """.format(meuble["prix"],meuble["id"]))
                print("<br/>")
else :
    print("Prise en compte de l'achat pour le compte : {} d'un montant de : {}€".format(form["numCompte"].value,form["montant"].value))
    rewriteJson()#on demande la sauvegarde de la base 
    try:
        r=requests.post(urlAchat,headers=Header,data=body.format(form["numCompte"].value,form["codeCompte"].value,form["montant"].value))
    except requests.exceptions.RequestException as e:
        print('connexion impossible au serveur ')
    
    if(r.status_code==200):# si la requete est un succee
        print("<br/>")
        print("achat confirmé, merci de votre confiance")
    else:
        print('erreur reponse serveur ')

print(""" <form action="/ecommerce.py" method="post">
       
        <input type="submit" name="send" value="retour au menu">
    </form> """)
            