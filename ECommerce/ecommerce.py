import cgitb
import cgi
import requests
from xml.etree import ElementTree as ET
import json
#cgitb.enable()

urlRemboursement='http://localhost:8080/APISOAP/services/Gateway'#url pour le webservice soap
Header = {"Content-Type":"text/xml","SOAPAction":""}
form = cgi.FieldStorage()#recuperation des données des formulaire


#partie remboursement
print("Content-Type: text/html")
print() 

print("<TITLE>Ecommerce</TITLE>")
print("<H1>Bienvenue sur notre site de ecommerce </H1>")
if "Refund" not in form:#si on est pas en mode remboursment
    print("<div>Si vous souhaitez vous faire rembourser, merci d'indiquer votre numero de compte ainsi que le montant</div>")
    refund=""" <form action="/ecommerce.py?Refund=1" method="post">
            <input required type="text" name="codeCompte" value=""/>
            <input required type="number" name="montant" value="0"/>
            <input type="submit" name="send" value="Envoyer votre demande de remboursement">
        </form> """
    print(refund)

    #partie achat 
    print("<br/><br/>")
    print("<div>")
    print("<table><tr>")
    with open('data.json') as json_data:
        data = json.load(json_data) # on recupere les donnée depuis la BDD 
        for meuble in data["meuble"]:
            if int(meuble['quantite'])>0 :
                print("<th>")
                print("""<image src="{}" width={} height={}/> """.format(meuble["image"],meuble["width"],meuble["height"]))
                print("<p>nom de l'objet :{}<br/>prix :{}<br/> quantité restante : {}</p>".format(meuble["nom"],meuble["prix"],meuble['quantite']))
                print(""" <form action="/achat.py?objet={}" method="post">       
                <input type="submit" name="send" value="acheter">
                </form> """.format(meuble["id"]))
                print("<br/>")
                print("<th/>")
            
    print("<tr/><table/>")
else:#si on est en mode remboursement 
    print(""" <form action="/ecommerce.py" method="post">
       
        <input type="submit" name="send" value="retour au menu">
    </form> """)

if "codeCompte" in form and "montant" in form and form["codeCompte"].value!="" and form["montant"].value!="" and "Refund" in form and form["Refund"].value=="1":
    bodyRefund = """
<soapenv:Envelope
	xmlns:q0="http://webservices"
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<soapenv:Header>
    	</soapenv:Header>
	<soapenv:Body>
    <q0:refund>
        <q0:numCompte>{}</q0:numCompte>
        <q0:montant>{}</q0:montant>
    </q0:refund>
	</soapenv:Body>
</soapenv:Envelope>""".format(form["codeCompte"].value,form["montant"].value)
    try:
        r=requests.post(urlRemboursement,headers=Header,data=bodyRefund)#envoie de la requete
    except requests.exceptions.RequestException as e:
        print('connexion impossible au serveur ')#si echec alors on le signale
    
    if(r.status_code==200 and ("succee" in r.text)):
        
        print("prise en compte de votre demande, vous avez etait remboursé de ",form["montant"].value)
        print("sur le compte ",form["codeCompte"].value)
    else:
        print('erreur reponse serveur ')



    


        