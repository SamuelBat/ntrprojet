package com;

import java.io.IOException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * 
 * @author samuel
 * classe permettant la manipulation de fichier Json
 *
 */
public class Json {

	public static ObjectMapper objectMapper= getDefaultObjectMapper();
	private static ObjectMapper getDefaultObjectMapper() {
		ObjectMapper defaultOM= new ObjectMapper();
		return 	defaultOM;
	}
	
	/**
	 * parse la source d'entrée en Json
	 * @param src string contenant du json
	 * @return src transformé en objet en json
	 */
	public static JsonNode parse(String src) {
		try {
			return objectMapper.readTree(src);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
}
