package com;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.tomcat.util.descriptor.tld.TagFileXml;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * 
 * @author samuel
 * classe permettant de manipuler du Xml
 */
public class Xml {

	/**
	 * retourne la valeur d'un tag dans un fichier Xml
	 * @param xml fichier Xml a lire 
	 * @param tagName nom de tag a rechercher
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static String getTagValue(String xml,String tagName) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		InputStream is =new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
		Document doc = dBuilder.parse(is);
		try {
		return doc.getElementsByTagName(tagName).item(0).getTextContent().trim();
		}catch (Exception e) {
			return null;
		}
	}
}
