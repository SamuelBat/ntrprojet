package com;

import javax.ws.rs.PathParam;
import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonNode;

@Path("/compte")
public class RestServer {
	static BDDManager bdd = new BDDManager();// accee à la bdd


	@GET
	@Path("/{id}/{code}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getCompte(@PathParam("id") String id, @PathParam("code") String code) {
		JsonNode compte = bdd.getCompte(id, code);
		if (compte != null) {
			return compte.toString();
		}
		return "pas de compte";
	}

	@POST
	@Consumes(MediaType.TEXT_XML)
	@Path("/achat")
	public String achat(String xml) {
		System.out.println("recu une demande");
		try {
			System.out.println("no here");
			String num = Xml.getTagValue(xml, "numCompte");
			String code = Xml.getTagValue(xml, "codeCompte");
			String montant = Xml.getTagValue(xml, "montant");
			JsonNode compte = bdd.getCompte(num, code);
			if (compte == null)
				return "pas de compte";
			if (compte.get("montant").asDouble() - Double.parseDouble(montant) >= 0) {
				bdd.saveData(num, compte.get("montant").asDouble() - Double.parseDouble(montant));
				return "achat valide";
			} else {
				return "pas assez d'argent sur le compte";
			}

		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "erreur de transfere";
	}

	@POST
	@Consumes(MediaType.TEXT_XML)
	@Path("/remboursement")
	public String remboursement(String xml) {
		try {
			System.out.println("lancement");
			String num = Xml.getTagValue(xml, "numCompte");
			String montant = Xml.getTagValue(xml, "montant");
			JsonNode compte = bdd.getCompte(num, "");
			if (compte == null) {
				return "pas de compte";
			}
			System.out.println("compte trouve");
			bdd.saveData(num, compte.get("montant").asDouble() + Double.parseDouble(montant));
			return "remboursement fait";

		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "erreur de transfere";
	}
}
