package com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
/**
 * 
 * @author samuel
 *
 */
public class BDDManager {

	private String dataPath;
	private JsonNode jsonTree;
	private boolean refresh = true;

	/**
	 * initialise la BDD 
	 */
	public BDDManager() {
		dataPath = "D:/samuel/eclipse-workspace/NTRProject/BanqueRest/bdd/data.json";
	}

	/**
	 * 
	 * @param numCompte numero de compte
	 * @param codeCompte code du compte (facultatif)
	 * @return compte si trouvé
	 */
	public JsonNode getCompte(String numCompte,String codeCompte) {
		System.out.println("je cherche le compte "+numCompte+" avec le code "+codeCompte);
		if(refresh) {
			refresh=false;
			openData();
		}
			for(JsonNode node : jsonTree.get("compte")) {
				if(node.get("numCompte").asText().contains(numCompte) && (codeCompte==null || codeCompte==""||node.get("codeCompte").asText().contains(codeCompte) ))
					return node;			
			}
			
			return null;
		
	}

	/**
	 * permet de lire les donnée de la base
	 */
	public void openData() {
		String myJson = "";

		BufferedReader lecteurAvecBuffer = null;
		String ligne;

		try {
			lecteurAvecBuffer = new BufferedReader(new FileReader(dataPath));
		} catch (FileNotFoundException exc) {
			System.out.println("Erreur d'ouverture");
		}
		try {
			while ((ligne = lecteurAvecBuffer.readLine()) != null)
				myJson += ligne;
		} catch (IOException e) {

			e.printStackTrace();
		}
		try {
			lecteurAvecBuffer.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
		try {
			lecteurAvecBuffer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		jsonTree = Json.parse(myJson);
	}

	/**
	 * sauvegarde les donnée dans la base en appliquant les modification sur un compte souhaité
	 * @param numCompte numero du compte a modifier
	 * @param montant nouveau montant a inscrire sur le compte
	 */
	public void saveData(String numCompte,double montant) {
		if(jsonTree==null || refresh) {
			openData();
		}

		ArrayList<JsonNode> liste = new ArrayList<JsonNode>();
		
		for(JsonNode jn : jsonTree.get("compte")) {
			if(jn.get("numCompte").asText().contains(numCompte)) {
				ObjectNode tmp = (ObjectNode)jn;
				tmp.put("montant", montant);
				liste.add(tmp);
			}else {
				liste.add(jn);
			}
			
		}
		
		File file = new File(dataPath);

	      if(file.delete()){//suppression du fichier
	       System.out.println(file.getName() + " est supprimé.");
	      }else{
	       System.out.println("Opération de suppression echouée");
	      }
		try
		{ //reecriture du fichier 
		 Files.write(Paths.get(dataPath), ("{\"compte\":"+liste.toString()+"}").getBytes(), StandardOpenOption.CREATE);
		}
		catch (IOException e)
		{
		 System.err.println("erreur dans l'ecriture de la base de données");
		}
		refresh=true;
		
		

	}


}
