import cgitb
import cgi
import requests
from xml.etree import ElementTree as ET
import json
#cgitb.enable()
urlConnexion="http://localhost:8080/BanqueRest/rest/compte/{}/{}"
header= {"Accept":"text/plain"}
print("Content-Type: text/html")
print() 
print("<title>Connexion à ma banque</title>")
form = cgi.FieldStorage()#recuperation des données des formulaire

if "valid" not in form:
    print("<h1>veuillez vous connecter</h1><br/>")
    print("saisissez vos information")

    print(""" <form action="/connexion.py?valid=1" method="post">
            <input required type="text" placeholder="numero de compte" name="numCompte"/>
            <input required type="password" placeholder="mot de passe" name="codeCompte"/>
        
            <input type="submit" name="send" value="connexion">
        </form> """)
else:
    print(""" <form action="/connexion.py" method="post">
            <input type="submit" name="send" value="deconnexion">
        </form> """)

    try:
        r=requests.get(urlConnexion.format(form["numCompte"].value,form["codeCompte"].value),headers=header)#envoie de la requete
    except requests.exceptions.RequestException as e:
        print('connexion impossible au serveur ')#si echec alors on le signale
    
    if(r.status_code==200):
        print("<h1>bienvenue sur votre compte</h1>")
        data=json.loads(r.text)
        print("<p> Mon numero de compte :{}".format(data["numCompte"]))
        print("<br/>credit disponible sur votre compte : {}".format(data["montant"]))
    else:
        print('erreur reponse serveur, essayez de vous reconnecter')
    
