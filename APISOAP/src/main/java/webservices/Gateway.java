package webservices;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import java.net.URL;
import java.nio.charset.StandardCharsets;

public class Gateway implements GatewayInt {

	@Override
	public String removeMoney(String numCompte,String codeCompte,double montant) {
		try {
			URL url = new URL("http://localhost:8080/BanqueRest/rest/compte/achat");
			HttpURLConnection http = (HttpURLConnection)url.openConnection();
			http.setRequestMethod("POST");
			http.setDoOutput(true);
			http.setRequestProperty("Content-Type", "text/xml");
			http.setRequestProperty("Accept", "text/xml");
			http.setRequestProperty("Accept", "tex/plain");

			String data = "<remboursement>\r\n"
					+ "    <numCompte>"+numCompte+"</numCompte>\r\n"
					+ "    <codeCompte>"+codeCompte+"</codeCompte>\r\n"
					+ "    <montant>"+montant+"</montant>\r\n"
					+ "</remboursement>";

			byte[] out = data.getBytes(StandardCharsets.UTF_8);

			OutputStream stream = http.getOutputStream();
			stream.write(out);

			System.out.println(http.getResponseCode() + " " + http.getResponseMessage());
			http.disconnect();

		} catch ( IOException e) {
			e.printStackTrace();
			return "erreur";
		}
		
		return "succee";
	}
	
	@Override
	public String refund(String numCompte,double montant) {
		
		try {
			URL url = new URL("http://localhost:8080/BanqueRest/rest/compte/remboursement");
			HttpURLConnection http = (HttpURLConnection)url.openConnection();
			http.setRequestMethod("POST");
			http.setDoOutput(true);
			http.setRequestProperty("Content-Type", "text/xml");
			http.setRequestProperty("Accept", "text/xml");
			http.setRequestProperty("Accept", "tex/plain");

			String data = "<achat>\r\n"
					+ "    <numCompte>"+numCompte+"</numCompte>\r\n"
					+ "    <montant>"+montant+"</montant>\r\n"
					+ "</achat>";

			byte[] out = data.getBytes(StandardCharsets.UTF_8);

			OutputStream stream = http.getOutputStream();
			stream.write(out);

			System.out.println(http.getResponseCode() + " " + http.getResponseMessage());
			http.disconnect();

		} catch ( IOException e) {
			e.printStackTrace();
			return "erreur";
		}
		
		return "succee";
	}


}
